# Overview
This is a small example how music publicly available on the internet can be used to quantify synaesthesia related data.

## Deezer
As first easiest implementation I chose Deezer. In script [query_deezer.py](deezer/query_deezer.py) we are searching for tracks which title contains specific color while incrementing BPM. This results in dataset containing:

- Track ID
- Track Title
- Track duration
- Track BPM
- Track Preview

These data can be further analyzed on adjectives used in track title to accompany the color. We have the tempo of song, its duration and musical preview which can be analyzed on frequency range.
