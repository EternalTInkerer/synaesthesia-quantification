import requests
import pandas as pd

genre_url = 'https://api.deezer.com/genre'
chart_url = 'https://api.deezer.com/chart'
search_url = 'https://api.deezer.com/search?q='
pop_id = 0
colors = ['red', 'green', 'blue', 'yellow']

################### DO NOT EDIT PAST THIS LINE!!! #####################

params = dict(
    q=''
)

for color in colors:
    BPM = 40
    song_table = []
    while BPM < 200:
        params['q'] = ('track:"%s" bpm_min:%d bpm_max=%d' % (color, BPM, BPM))
        search_resp = requests.get(url=search_url, params=params)
        songs = search_resp.json()['data']
        for song in songs:
            song_table.append({'id':int(song['id']), 'title':song['title'], 'color':color, 'bpm':BPM, 'duration':song['duration'], 'preview':song['preview']})
        BPM += 1
    song_frame = pd.DataFrame(song_table)
    song_frame.to_csv('songs_by_bpm_and_duration_%s.csv' % color)
