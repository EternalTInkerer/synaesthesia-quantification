import requests
import pandas as pd

genre_url = 'https://api.deezer.com/genre'
chart_url = 'https://api.deezer.com/chart'
search_url = 'https://api.deezer.com/search?q='
pop_id = 0

genre_resp = requests.get(url=genre_url)

for genre in genre_resp.json()['data']:
    if genre['name'] == 'Pop':
        pop_id = genre['id']
        break

pop_chart_url = chart_url + '/' + str(pop_id)
chart_resp = requests.get(url=pop_chart_url)

songs = []

for chart_song in chart_resp.json():
    print()
    #print(params)